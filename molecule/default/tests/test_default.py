import os

import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all') 


@pytest.fixture()
def AnsibleVars(host):
    all_vars =  host.ansible.get_variables()
    return all_vars

def test_mongo_package_installed(host,AnsibleVars):
    pkg = host.package('mongodb-org')

    assert pkg.is_installed
    assert pkg.version == AnsibleVars['urlsh_mongo_version']

def test_mongo_process(host, AnsibleVars):
    p = host.process.get(user="mongodb", comm="mongod")

def test_mongo_conf_file(host, AnsibleVars):
    f = host.file('/etc/mongod.conf')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'



def test_mongo_port(host, AnsibleVars):
    port = host.socket("tcp://127.0.0.1:{}".format(AnsibleVars['urlsh_mongo_server_port'])).is_listening

def test_node_package_installed(host):
    pkg = host.package('nodejs')

    assert pkg.is_installed
    assert pkg.version == "8.17.0-1nodesource1"

def test_app_user(host, AnsibleVars):
    u = host.user(AnsibleVars['urlsh_app_linux_user'])

    assert u.exists
    

def test_app_dir_deployment(host, AnsibleVars):
    f = host.file(AnsibleVars['urlsh_app_svar_path'])
    
    assert f.exists
    assert f.is_directory
    assert f.user == AnsibleVars['urlsh_app_linux_user']
    

def test_app_deployment(host, AnsibleVars):
    f = host.file("{}/app.js".format(AnsibleVars['urlsh_app_svar_path']))
    
    assert f.exists
    assert f.is_file
    assert f.user == AnsibleVars['urlsh_app_linux_user']


# This is left intentionally to be done during handson 
# def test_pm2_ecosystem_file(host,AnsibleVars):
# def test_app_port(host,AnsibleVars):
# def test_nginx_pkg(host,AnsibleVars):
# def test_nginx_version(host,AnsibleVars):
# def test_nginx_vhost(host,AnsibleVars):
# def test_nginx_vhostenabled(host,AnsibleVars):
# def test_nginx_port(host,AnsibleVars):

